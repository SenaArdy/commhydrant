package com.ptfi.commhydrant.Models;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class ComplianceModel {

    public long id;
    public String findings;
    public String remark;
    public String responsibility;
    public String done;
    public String equipment;
    public String date;
    public String register;

    public ComplianceModel() {
        this.id = -1;
        this.findings = "";
        this.remark = "";
        this.responsibility = "";
        this.done = "";
        this.equipment = "";
        this.date = "";
        this.register = "";
    }

    public ComplianceModel(long id, String findings, String remark, String responsibility, String done, String equipment, String date, String register) {
        this.id = id;
        this.findings = findings;
        this.remark = remark;
        this.responsibility = responsibility;
        this.done = done;
        this.equipment = equipment;
        this.date = date;
        this.register = register;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }
}
