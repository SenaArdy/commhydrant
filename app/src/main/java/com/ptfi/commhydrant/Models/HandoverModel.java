package com.ptfi.commhydrant.Models;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class HandoverModel {

    public long id;
    public String equipment;
    public String location;
    public String client;
    public String date;
    public String type;
    public String register;
    public String nameEng;
    public String idEng;
    public byte[] signEng;
    public String nameMain;
    public String idMain;
    public byte[] signMain;
    public String nameAO;
    public String idAO;
    public byte[] signAO;
    public String nameUGMR;
    public String idUGMR;
    public byte[] signUGMR;
    public String nameCSE;
    public String idCSE;
    public byte[] signCSE;
    public String nameDept;
    public String idDept;
    public byte[] signDept;
    public String division;
    public String contractor;

    public HandoverModel() {
        this.id = -1;
        this.equipment = "";
        this.location = "";
        this.client = "";
        this.date = "";
        this.type = "";
        this.register = "";
        this.nameEng = "";
        this.idEng = "";
        this.signEng = null;
        this.nameMain = "";
        this.idMain = "";
        this.signMain = null;
        this.nameAO = "";
        this.idAO = "";
        this.signAO = null;
        this.nameUGMR = "";
        this.idUGMR = "";
        this.signUGMR = null;
        this.nameCSE = "";
        this.idCSE = "";
        this.signCSE = null;
        this.nameDept = "";
        this.idDept = "";
        this.signDept = null;
        this.division = "";
        this.contractor = "";
    }

    public HandoverModel(long id, String equipment, String location, String client, String date, String type, String register, String nameEng,
                         String idEng, byte[] signEng, String nameMain, String idMain, byte[] signMain, String nameAO, String idAO, byte[] signAO,
                         String nameUGMR, String idUGMR, byte[] signUGMR, String nameCSE, String idCSE, byte[] signCSE, String nameDept,
                         String idDept, byte[] signDept, String division, String contractor) {
        this.id = id;
        this.equipment = equipment;
        this.location = location;
        this.client = client;
        this.date = date;
        this.type = type;
        this.register = register;
        this.nameEng = nameEng;
        this.idEng = idEng;
        this.signEng = signEng;
        this.nameMain = nameMain;
        this.idMain = idMain;
        this.signMain = signMain;
        this.nameAO = nameAO;
        this.idAO = idAO;
        this.signAO = signAO;
        this.nameUGMR = nameUGMR;
        this.idUGMR = idUGMR;
        this.signUGMR = signUGMR;
        this.nameCSE = nameCSE;
        this.idCSE = idCSE;
        this.signCSE = signCSE;
        this.nameDept = nameDept;
        this.idDept = idDept;
        this.signDept = signDept;
        this.division = division;
        this.contractor = contractor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getIdEng() {
        return idEng;
    }

    public void setIdEng(String idEng) {
        this.idEng = idEng;
    }

    public byte[] getSignEng() {
        return signEng;
    }

    public void setSignEng(byte[] signEng) {
        this.signEng = signEng;
    }

    public String getNameMain() {
        return nameMain;
    }

    public void setNameMain(String nameMain) {
        this.nameMain = nameMain;
    }

    public String getIdMain() {
        return idMain;
    }

    public void setIdMain(String idMain) {
        this.idMain = idMain;
    }

    public byte[] getSignMain() {
        return signMain;
    }

    public void setSignMain(byte[] signMain) {
        this.signMain = signMain;
    }

    public String getNameAO() {
        return nameAO;
    }

    public void setNameAO(String nameAO) {
        this.nameAO = nameAO;
    }

    public String getIdAO() {
        return idAO;
    }

    public void setIdAO(String idAO) {
        this.idAO = idAO;
    }

    public byte[] getSignAO() {
        return signAO;
    }

    public void setSignAO(byte[] signAO) {
        this.signAO = signAO;
    }

    public String getNameUGMR() {
        return nameUGMR;
    }

    public void setNameUGMR(String nameUGMR) {
        this.nameUGMR = nameUGMR;
    }

    public String getIdUGMR() {
        return idUGMR;
    }

    public void setIdUGMR(String idUGMR) {
        this.idUGMR = idUGMR;
    }

    public byte[] getSignUGMR() {
        return signUGMR;
    }

    public void setSignUGMR(byte[] signUGMR) {
        this.signUGMR = signUGMR;
    }

    public String getNameCSE() {
        return nameCSE;
    }

    public void setNameCSE(String nameCSE) {
        this.nameCSE = nameCSE;
    }

    public String getIdCSE() {
        return idCSE;
    }

    public void setIdCSE(String idCSE) {
        this.idCSE = idCSE;
    }

    public byte[] getSignCSE() {
        return signCSE;
    }

    public void setSignCSE(byte[] signCSE) {
        this.signCSE = signCSE;
    }

    public String getNameDept() {
        return nameDept;
    }

    public void setNameDept(String nameDept) {
        this.nameDept = nameDept;
    }

    public String getIdDept() {
        return idDept;
    }

    public void setIdDept(String idDept) {
        this.idDept = idDept;
    }

    public byte[] getSignDept() {
        return signDept;
    }

    public void setSignDept(byte[] signDept) {
        this.signDept = signDept;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }
}
