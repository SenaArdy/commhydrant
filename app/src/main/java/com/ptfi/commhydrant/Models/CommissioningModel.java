package com.ptfi.commhydrant.Models;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class CommissioningModel {

    public long id;
    public String question;
    public String answer;
    public String remarks;
    public String equipment;
    public String date;
    public String register;

    public CommissioningModel() {
        this.id = -1;
        this.question = "";
        this.answer = "";
        this.remarks = "";
        this.equipment = "";
        this.date = "";
        this.register = "";
    }

    public CommissioningModel(long id, String question, String answer, String remarks, String equipment, String date, String register) {
        this.id = id;
        this.question = question;
        this.answer = answer;
        this.remarks = remarks;
        this.equipment = equipment;
        this.date = date;
        this.register = register;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }
}
