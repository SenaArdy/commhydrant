package com.ptfi.commhydrant.Models;

/**
 * Created by senaardyputra on 7/30/16.
 */
public class InspectorModel {

    public long id;
    public String nameIns;
    public String idIns;
    public String titleIns;
    public String orgIns;

    public InspectorModel() {
        this.id = -1;
        this.nameIns = "";
        this.idIns = "";
        this.titleIns = "";
        this.orgIns = "";
    }

    public InspectorModel(long id, String nameIns, String idIns, String titleIns, String orgIns) {
        this.id = id;
        this.nameIns = nameIns;
        this.idIns = idIns;
        this.titleIns = titleIns;
        this.orgIns = orgIns;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameIns() {
        return nameIns;
    }

    public void setNameIns(String nameIns) {
        this.nameIns = nameIns;
    }

    public String getIdIns() {
        return idIns;
    }

    public void setIdIns(String idIns) {
        this.idIns = idIns;
    }

    public String getTitleIns() {
        return titleIns;
    }

    public void setTitleIns(String titleIns) {
        this.titleIns = titleIns;
    }

    public String getOrgIns() {
        return orgIns;
    }

    public void setOrgIns(String orgIns) {
        this.orgIns = orgIns;
    }
}
