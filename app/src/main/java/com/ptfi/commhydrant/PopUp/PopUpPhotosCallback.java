package com.ptfi.commhydrant.PopUp;

import com.ptfi.commhydrant.Models.PhotoModel;

/**
 * Created by senaardyputra on 7/18/16.
 */
public interface PopUpPhotosCallback {
    public void onDataSave(PhotoModel model);
}
