package com.ptfi.commhydrant.PopUp;

import com.ptfi.commhydrant.Models.CommissioningModel;
import com.ptfi.commhydrant.Models.ComplianceModel;

/**
 * Created by senaardyputra on 7/30/16.
 */
public interface PopUpCommTestCallBack {
    public void onDataSave(CommissioningModel model);
}
