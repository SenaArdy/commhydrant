package com.ptfi.commhydrant.PopUp;

import com.ptfi.commhydrant.Models.ComplianceModel;

/**
 * Created by senaardyputra on 6/18/16.
 */
public interface PopUpComplianceCallback {
    public void onDataSave(ComplianceModel model);
}
