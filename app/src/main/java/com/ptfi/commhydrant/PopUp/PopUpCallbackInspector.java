package com.ptfi.commhydrant.PopUp;

import com.ptfi.commhydrant.Models.HandoverModel;
import com.ptfi.commhydrant.Models.HydrantModel;

/**
 * Created by senaardyputra on 7/27/16.
 */
public interface PopUpCallbackInspector {
    public void onDataSave(HydrantModel model, String name, String division);

    public void onDataSaveHO(HandoverModel model, String name, String division);
}
