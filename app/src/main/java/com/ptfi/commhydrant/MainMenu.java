package com.ptfi.commhydrant;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ptfi.commhydrant.Utils.Helper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by senaardyputra on 7/30/16.
 */
public class MainMenu extends AppCompatActivity {

    Activity mActivity = MainMenu.this;
    CardView inspectionCV, sheInspectionCV;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_mainmenu);

        ButterKnife.inject(MainMenu.this);
        setSupportActionBar(toolbar);

        initComponent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        customActionbar();
        return true;
    }

    private void customActionbar() {
        LayoutInflater customBar = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = customBar.inflate(R.layout.activity_all_pages, null);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        actionbar.setCustomView(actionBarView);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_contact2:
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//                return true;
//
//            case R.id.action_contact:
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public void aboutChangelog(View v) {
//        QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//    }

    public void initComponent() {
        inspectionCV = (CardView) findViewById(R.id.inspectionCV);
        sheInspectionCV = (CardView) findViewById(R.id.sheInspectionCV);

        inspectionCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, PagerSlidingMenu.class);
                startActivity(intent);
                finish();
            }
        });

        sheInspectionCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
                builder.setIcon(R.drawable.warning_logo);
                builder.setTitle("Open SHE Inspection Application?");
                builder.setMessage("It will close from this application and Open SHE Inspection Application");

                builder.setPositiveButton("AGREE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName("com.dios.she",
                                "com.dios.she.MainActivity"));
                        if (Helper.isAvailableIntent(MainMenu.this,
                                intent)) {
                            intent.putExtra("isOtherApp", true);
                            startActivity(intent);
                        } else {
                            Helper
                                    .showPopUpMessage(
                                            MainMenu.this,
                                            "Warning",
                                            "You don't have SHE Inspection application, you must install SHE Inspection application",
                                            null);
                        }
                    }
                });

                builder.setNegativeButton("DISAGREE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();
            }
        });
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Hydrant Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });
    }
}
