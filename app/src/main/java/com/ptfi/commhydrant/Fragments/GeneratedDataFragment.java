package com.ptfi.commhydrant.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ptfi.commhydrant.Adapters.ListGenerateDataMainAdapter;
import com.ptfi.commhydrant.Adapters.ListGeneratedDataHandOverAdapter;
import com.ptfi.commhydrant.Database.DataSource;
import com.ptfi.commhydrant.MainMenu;
import com.ptfi.commhydrant.Models.DataSingleton;
import com.ptfi.commhydrant.Models.HandoverModel;
import com.ptfi.commhydrant.Models.HydrantModel;
import com.ptfi.commhydrant.Models.InspectorModel;
import com.ptfi.commhydrant.PopUp.GeneratePopUp;
import com.ptfi.commhydrant.PopUp.PopUp;
import com.ptfi.commhydrant.R;
import com.ptfi.commhydrant.Utils.Helper;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class GeneratedDataFragment extends Fragment {

    static final int REQUEST_SCAN_BARCODE_MAIN = 349;
    static int REQUEST_SCAN_BARCODE_DEPT = 353;

    static int REQUEST_SCAN_BARCODE_MAIN_HO = 356;
    static int REQUEST_SCAN_BARCODE_DEPT_HO = 360;

    protected static FragmentActivity mActivity;

    EditText equipmentET, dateET;
    TextInputLayout equipmentTL, dateTL;
    TextView imeiCode, versionCode;
    RecyclerView.LayoutManager layoutManager;

    static String inspectorIDFalse;
    private PopUp.page pages;
    private PopUp.signature signatures;

    static ArrayList<InspectorModel> dataInspector;

    CardView searchCV;

    RecyclerView rV;

    MultiStateToggleButton pagesMT;

    ListGenerateDataMainAdapter sprinklerAdapter;
    ListGeneratedDataHandOverAdapter handOverAdapter;
    private static ArrayList<HydrantModel> sprinklerData = new ArrayList<>();
    private static ArrayList<HandoverModel> handOverData = new ArrayList<>();

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_all_generated_data, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PopUp.pageComm = PopUp.page.generateData;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        initComponent(rootView);

        return rootView;
    }

    public void initComponent(View rootView) {
        if (rootView != null) {
            equipmentET = (EditText) rootView.findViewById(R.id.equipmentET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);

            pagesMT = (MultiStateToggleButton) rootView.findViewById(R.id.pagesMT);

            equipmentTL = (TextInputLayout) rootView.findViewById(R.id.equipmentTL);
            dateTL= (TextInputLayout) rootView.findViewById(R.id.dateTL);

            searchCV = (CardView) rootView.findViewById(R.id.searchCV);

            rV = (RecyclerView) rootView.findViewById(R.id.rV);

            // Date
            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
        }

        searchCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataSource ds = new DataSource(mActivity);
                ds.open();

                if (pagesMT.getValue() == 0) {
                    if (equipmentET.getText().toString().length() > 0
                            && dateET.getText().toString().length() == 0) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        sprinklerData = ds.getDataCommissioningFromEquipment(equipmentET.getText().toString());

                        sprinklerAdapter = new ListGenerateDataMainAdapter(mActivity, sprinklerData);
                        rV.setAdapter(sprinklerAdapter);

                    } else if (dateET.getText().toString().length() > 0
                            && equipmentET.getText().toString().length() == 0) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        sprinklerData = ds.getDataCommissioningFromDate(dateET.getText().toString());

                        sprinklerAdapter = new ListGenerateDataMainAdapter(mActivity, sprinklerData);
                        rV.setAdapter(sprinklerAdapter);

                    } else if (equipmentET.getText().toString().length() > 0
                            && dateET.getText().toString().length() > 0) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        sprinklerData = ds.getDataCommissioningFromEquipmentAndDate(equipmentET.getText().toString(),
                                dateET.getText().toString());

                        sprinklerAdapter = new ListGenerateDataMainAdapter(mActivity, sprinklerData);
                        rV.setAdapter(sprinklerAdapter);

                    }
                } else {
                    if (equipmentET.getText().toString().length() > 1
                            && dateET.getText().toString().length() == 0) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        handOverData = ds.getDataHandOverFromEquipment(equipmentET.getText().toString());

                        handOverAdapter = new ListGeneratedDataHandOverAdapter(mActivity, handOverData);
                        rV.setAdapter(handOverAdapter);

                    } else if (dateET.getText().toString().length() > 1
                            && equipmentET.getText().toString().length() == 0) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        handOverData = ds.getDataHandOverFromDate(dateET.getText().toString());

                        handOverAdapter = new ListGeneratedDataHandOverAdapter(mActivity, handOverData);
                        rV.setAdapter(handOverAdapter);

                    } else if (equipmentET.getText().toString().length() > 1
                            && dateET.getText().toString().length() > 1) {

                        layoutManager = new LinearLayoutManager(getActivity());
                        rV.setLayoutManager(layoutManager);
                        rV.setItemAnimator(new DefaultItemAnimator());

                        handOverData = ds.getDataHandOverFromEquipmentAndDate(equipmentET.getText().toString(), dateET.getText().toString());

                        handOverAdapter = new ListGeneratedDataHandOverAdapter(mActivity, handOverData);
                        rV.setAdapter(handOverAdapter);

                    }
                }
                ds.close();
            }
        });
    }

    public static void datePickersGenerated(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        pages = PopUp.page.handover;

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            GeneratePopUp.maintenanceET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if (name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.maintenance;
                pages = PopUp.page.commissioning;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, GeneratePopUp.equipmentET.getText().toString(), GeneratePopUp.dateET.getText().toString(),
                        GeneratePopUp.registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            GeneratePopUp.deptHeadET.setText(ds.getNameFromID(inspectorID));

            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.deptHead;
                pages = PopUp.page.commissioning;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, GeneratePopUp.equipmentET.getText().toString(), GeneratePopUp.dateET.getText().toString(),
                        GeneratePopUp.registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN_HO
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            GeneratePopUp.maintenanceET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if (name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.maintenance;
                pages = PopUp.page.handover;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, GeneratePopUp.equipmentET.getText().toString(), GeneratePopUp.dateET.getText().toString(),
                        GeneratePopUp.registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN_HO
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_DEPT_HO
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            GeneratePopUp.deptHeadET.setText(ds.getNameFromID(inspectorID));

            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.deptHead;
                pages = PopUp.page.handover;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, GeneratePopUp.equipmentET.getText().toString(), GeneratePopUp.dateET.getText().toString(),
                        GeneratePopUp.registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_DEPT_HO
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Hydrant Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(mActivity, MainMenu.class);
                mActivity.startActivity(intent);
                mActivity.finish();
            }
        });

        builder.show();
    }
}
