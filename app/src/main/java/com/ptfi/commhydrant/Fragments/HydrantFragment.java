package com.ptfi.commhydrant.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commhydrant.Adapters.ListCommTestAdapter;
import com.ptfi.commhydrant.Adapters.ListComplianceAdapter;
import com.ptfi.commhydrant.Adapters.ListPhotoAdapter;
import com.ptfi.commhydrant.Database.DataSource;
import com.ptfi.commhydrant.Models.CommissioningModel;
import com.ptfi.commhydrant.Models.ComplianceModel;
import com.ptfi.commhydrant.Models.DataSingleton;
import com.ptfi.commhydrant.Models.HandoverModel;
import com.ptfi.commhydrant.Models.HydrantModel;
import com.ptfi.commhydrant.Models.InspectorModel;
import com.ptfi.commhydrant.Models.PhotoModel;
import com.ptfi.commhydrant.PopUp.PhotoPopUp;
import com.ptfi.commhydrant.PopUp.PopUp;
import com.ptfi.commhydrant.PopUp.PopUpCallbackInspector;
import com.ptfi.commhydrant.PopUp.PopUpCommTestCallBack;
import com.ptfi.commhydrant.PopUp.PopUpComplianceCallback;
import com.ptfi.commhydrant.PopUp.PopUpPhotosCallback;
import com.ptfi.commhydrant.R;
import com.ptfi.commhydrant.Utils.FoldersFilesName;
import com.ptfi.commhydrant.Utils.Helper;
import com.ptfi.commhydrant.Utils.LanguageConstants;
import com.ptfi.commhydrant.Utils.Report;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class HydrantFragment extends Fragment {

    protected static FragmentActivity mActivity;

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static final int REQUEST_SCAN_BARCODE_CLIENT = 347;
    static final int REQUEST_SCAN_BARCODE_ENG = 348;
    static final int REQUEST_SCAN_BARCODE_MAIN = 349;
    static final int REQUEST_SCAN_BARCODE_AO = 350;
    static final int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static final int REQUEST_SCAN_BARCODE_CSE = 352;
    static final int REQUEST_SCAN_BARCODE_DEPT = 353;

    public PhotoModel activeFModel = null;

    private Dialog activeDialog;

    static String inspectorIDFalse;

    ArrayList<InspectorModel> dataInspector;

    static EditText equipmentET, dateET, clientET, typeET, registerET, locationET, remark1ET, remark2ET, remark3ET, remark4ET, engineeringET, maintenanceET,
            areaOwnerET, ugmrET, cseET, deptHeadET;

    static TextInputLayout equipmentTL, dateTL, clientTL, typeTL, registerTL, locationTL, remark1TL, remark2TL, remark3TL, remark4TL, engineeringTL, maintenanceTL,
            areaOwnerTL, ugmrTL, cseTL, deptHeadTL;

    static MultiStateToggleButton insp1, insp2, insp3, insp4;

    static TextView Q1TV, Q2TV, Q3TV, Q4TV, Q80TV, Q90TV, Q100TV, Q110TV;

    static ImageView addComplianceIV, addPhotosIV, addCommTestIV;

    static LinearLayout findingsLL, photosLL;

    static RecyclerView findingsRV, photosRV, commTestRV;

    TextView imeiCode, versionCode;

    static CardView generateCV, previewCV;

    private PopUp.page pages;
    private PopUp.signature signatures;

    static RecyclerView.LayoutManager layoutManager;
    static RecyclerView.LayoutManager photosLayoutManager;

    private static ArrayList<CommissioningModel> commissioningData = new ArrayList<>();
    private static ArrayList<ComplianceModel> complianceData = new ArrayList<>();
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();

    static ListCommTestAdapter commData;
    static ListComplianceAdapter adapterData;
    static ListPhotoAdapter adapterPhotosData;

    LanguageConstants languageConstants;
    android.widget.ToggleButton languageToggleButton;
    boolean isChangedLanguage;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PopUp.pageComm = PopUp.page.commissioning;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        initComponent(rootView);
        commTestRv();
        findingRv();
        photosRv();
        setListener();

        return rootView;
    }

    public void initComponent(final View rootView) {
        if (rootView != null) {
            equipmentET = (EditText) rootView.findViewById(R.id.equipmentET);
            registerET = (EditText) rootView.findViewById(R.id.registerET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            clientET = (EditText) rootView.findViewById(R.id.clientET);
            typeET = (EditText) rootView.findViewById(R.id.typeEqET);
            locationET = (EditText) rootView.findViewById(R.id.locationEqET);

            equipmentTL = (TextInputLayout) rootView.findViewById(R.id.equipmentTL);
            registerTL = (TextInputLayout) rootView.findViewById(R.id.registerTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            clientTL = (TextInputLayout) rootView.findViewById(R.id.clientTL);
            typeTL = (TextInputLayout) rootView.findViewById(R.id.typeEqTL);
            locationTL = (TextInputLayout) rootView.findViewById(R.id.locationEqTL);

            engineeringET = (EditText) rootView.findViewById(R.id.engineeringET);
            maintenanceET = (EditText) rootView.findViewById(R.id.maintenanceET);
            areaOwnerET = (EditText) rootView.findViewById(R.id.areaOwnerET);
            ugmrET = (EditText) rootView.findViewById(R.id.ugmrET);
            cseET = (EditText) rootView.findViewById(R.id.cseET);
            deptHeadET = (EditText) rootView.findViewById(R.id.deptHeadET);

            engineeringTL = (TextInputLayout) rootView.findViewById(R.id.engineeringTL);
            maintenanceTL = (TextInputLayout) rootView.findViewById(R.id.maintenanceTL);
            areaOwnerTL = (TextInputLayout) rootView.findViewById(R.id.areaOwnerTL);
            ugmrTL = (TextInputLayout) rootView.findViewById(R.id.ugmrTL);
            cseTL = (TextInputLayout) rootView.findViewById(R.id.cseTL);
            deptHeadTL = (TextInputLayout) rootView.findViewById(R.id.deptHeadTL);

            insp1 = (MultiStateToggleButton) rootView.findViewById(R.id.insp1);
            insp2 = (MultiStateToggleButton) rootView.findViewById(R.id.insp2);
            insp3 = (MultiStateToggleButton) rootView.findViewById(R.id.insp3);
            insp4 = (MultiStateToggleButton) rootView.findViewById(R.id.insp4);

            remark1TL = (TextInputLayout) rootView.findViewById(R.id.remark1TL);
            remark2TL = (TextInputLayout) rootView.findViewById(R.id.remark2TL);
            remark3TL = (TextInputLayout) rootView.findViewById(R.id.remark3TL);
            remark4TL = (TextInputLayout) rootView.findViewById(R.id.remark4TL);

            remark1ET = (EditText) rootView.findViewById(R.id.remark1ET);
            remark2ET = (EditText) rootView.findViewById(R.id.remark2ET);
            remark3ET = (EditText) rootView.findViewById(R.id.remark3ET);
            remark4ET = (EditText) rootView.findViewById(R.id.remark4ET);

            Q1TV = (TextView) rootView.findViewById(R.id.Q1TV);
            Q2TV = (TextView) rootView.findViewById(R.id.Q2TV);
            Q3TV = (TextView) rootView.findViewById(R.id.Q3TV);
            Q4TV = (TextView) rootView.findViewById(R.id.Q4TV);

            Q80TV = (TextView) rootView.findViewById(R.id.Q80TV);
            Q90TV = (TextView) rootView.findViewById(R.id.Q90TV);
            Q100TV = (TextView) rootView.findViewById(R.id.Q100TV);
            Q110TV = (TextView) rootView.findViewById(R.id.Q110TV);

            activeFModel = new PhotoModel();

            addCommTestIV = (ImageView) rootView.findViewById(R.id.addCommTestIV);
            addCommTestIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopUp.showCommTest(mActivity, equipmentET.getText().toString(), dateET.getText().toString(), registerET.getText().toString(),
                            null, null, null, true, new PopUpCommTestCallBack() {
                                @Override
                                public void onDataSave(CommissioningModel model) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();

                                    ds.insertComm(model);
                                    commTestRv();

                                    ds.close();
                                }
                            });
                }
            });

            addComplianceIV = (ImageView) rootView.findViewById(R.id.addComplianceIV);
            addComplianceIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopUp.showCompliance(mActivity, equipmentET.getText().toString(), dateET.getText().toString(), registerET.getText().toString(),
                            null, null, null, null, true, new PopUpComplianceCallback() {
                                @Override
                                public void onDataSave(ComplianceModel model) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();

                                    ds.insertCompliance(model);
                                    findingRv();

                                    ds.close();
                                }
                            });
                }
            });

            addPhotosIV = (ImageView) rootView.findViewById(R.id.addPhotosIV);

            addPhotosIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PhotoPopUp.showAddImagePopUp(mActivity, equipmentET.getText().toString(),
                            dateET.getText().toString(), registerET.getText().toString(), new PopUpPhotosCallback() {
                                @Override
                                public void onDataSave(PhotoModel model) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();

                                    ds.insertPhotos(model);
                                    photosRv();

                                    ds.close();
                                }
                            });
                }
            });

            addPhotosIV = (ImageView) rootView.findViewById(R.id.addPhotosIV);

            findingsLL = (LinearLayout) rootView.findViewById(R.id.findingsLL);
            photosLL = (LinearLayout) rootView.findViewById(R.id.photosLL);

            commTestRV = (RecyclerView) rootView.findViewById(R.id.commTestRV);
            findingsRV = (RecyclerView) rootView.findViewById(R.id.findingsRV);
            photosRV = (RecyclerView) rootView.findViewById(R.id.photosRV);

            generateCV = (CardView) rootView.findViewById(R.id.generateCV);
            previewCV = (CardView) rootView.findViewById(R.id.previewCV);

            generateCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validationGenerate()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        }
                        ds.close();
                    }
                }
            });

            previewCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validationGenerate()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        }
                        ds.close();
                    }
                }
            });

            Q80TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(findingsRV.getVisibility() == View.GONE) {
                        findingsRV.setVisibility(View.VISIBLE);
                    } else if ( findingsRV.getVisibility() == View.VISIBLE) {
                        findingsRV.setVisibility(View.GONE);
                    }
                }
            });

            Q90TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(photosRV.getVisibility() == View.GONE) {
                        photosRV.setVisibility(View.VISIBLE);
                    } else if ( photosRV.getVisibility() == View.VISIBLE) {
                        photosRV.setVisibility(View.GONE);
                    }
                }
            });

            engineeringET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            maintenanceET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            areaOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            ugmrET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final  DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            cseET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });

                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HydrantModel model, String name, String division) {
                                            ds.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            deptHeadET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());

            languageToggleButton = (android.widget.ToggleButton) rootView.findViewById(R.id.toggleBtnLanguange);

            isChangedLanguage = false;
        }
    }

    public void setListener() {
        languageToggleButton
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        String language = "";
                        if (isChecked) {
                            language = "English Language";
                        } else {
                            language = "Bahasa Indonesia";
                        }
                        setLanguage(isChecked);
                        Toast.makeText(mActivity, language,
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void setLanguage(boolean isEnglish) {
        if (isEnglish) {
            languageConstants = new LanguageConstants(false);
        } else {
            languageConstants = new LanguageConstants(true);
        }

        Q1TV.setText(languageConstants.Q1);
        Q2TV.setText(languageConstants.Q2);
        Q3TV.setText(languageConstants.Q3);
        Q4TV.setText(languageConstants.Q4);

        isChangedLanguage = true;
    }

    public static boolean validation() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Hydrant Equipment");
            emptyFields += "Please fill Hydrant Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Hydrant Location");
            emptyFields += "Please fill Hydrant Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Hydrant Type");
            emptyFields += "Please fill Hydrant Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

        if (insp1.getValue() == 1){
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.1\n";
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.1\n";
        }

        if (insp2.getValue() == 1){
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.2\n";
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.2\n";
        }

        if (insp3.getValue() == 1){
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.3\n";
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.3\n";
        }

        if (insp4.getValue() == 1){
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.4\n";
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.4\n";
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Hydrant Register");
//        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }

    public static boolean validationClient() {
        boolean status = true;

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Hydrant Equipment");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Hydrant Location");
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Hydrant Type");
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Hydrant Register");
//        }

        return status;
    }

    public static boolean validationGenerate() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Hydrant Equipment");
            emptyFields += "Please fill Hydrant Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Hydrant Location");
            emptyFields += "Please fill Hydrant Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Hydrant Type");
            emptyFields += "Please fill Hydrant Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

        if (insp1.getValue() == 1){
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.1\n";
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.1\n";
        }

        if (insp2.getValue() == 1){
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.2\n";
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.2\n";
        }

        if (insp3.getValue() == 1){
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.3\n";
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.3\n";
        }

        if (insp4.getValue() == 1){
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("*Please fill remark if you choose 'No'");
                emptyFields += "Please remark if you choose 'No' No.4\n";
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("*Please fill Inspection Choice");
            emptyFields += "Please fill Inspection Choice No.4\n";
        }

        if (engineeringET.getText().toString().length() == 0) {
            status = false;
            engineeringTL.setErrorEnabled(true);
            engineeringTL.setError("*Please fill Engineering Name");
            emptyFields += "Please fill Engineering Name\n";
        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }

    public static void commTestRv() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        commTestRV.setLayoutManager(layoutManager);
        commTestRV.setItemAnimator(new DefaultItemAnimator());

        commissioningData = ds.getAllCommEquipment(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        commData = new ListCommTestAdapter(mActivity, commissioningData);
        commTestRV.setAdapter(commData);
        ds.close();
    }

    public static void findingRv() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        findingsRV.setLayoutManager(layoutManager);
        findingsRV.setItemAnimator(new DefaultItemAnimator());

        complianceData = ds.getAllComplianceEquipment(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        adapterData = new ListComplianceAdapter(mActivity, complianceData);
        findingsRV.setAdapter(adapterData);
        ds.close();
    }

    public static void photosRv() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        photosLayoutManager = new LinearLayoutManager(mActivity);
        photosRV.setLayoutManager(photosLayoutManager);
        photosRV.setItemAnimator(new DefaultItemAnimator());

        photoData = ds.getAllPhotoData(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
        photosRV.setAdapter(adapterPhotosData);
        ds.close();
    }

    public static void datePickers(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
                case engineering:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG);
                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT);
                    break;

//                case client:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CLIENT);
//                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        pages = PopUp.page.commissioning;

        if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            engineeringET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.engineering;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            maintenanceET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.maintenance;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            areaOwnerET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.areaOwner;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            ugmrET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.mainRes;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            cseET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.CSE;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        }else if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            deptHeadET.setText(ds.getNameFromID(inspectorID));

            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.deptHead;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            clientET.setText(ds.getNameFromID(inspectorID));
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == PhotoPopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {

            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(PhotoPopUp.getPicture(), Helper.fileUri.getPath());
                PhotoPopUp.setImagePath(Helper.fileUri.getPath());
            }
        } else if (requestCode == PhotoPopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            PhotoPopUp.setImagePath(picturePath);
            Helper.setPic(PhotoPopUp.getPicture(), picturePath);
        }

        if (requestCode == Report.OPEN_PREVIEW) {
            File fileName = new File(
                    FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/"
                            + new SimpleDateFormat("yyyyMMdd")
                            .format(new Date()) + "/"
                            + "Commissioning Hydrant.pdf");
            if (fileName.exists())
                fileName.delete();
        }
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        HydrantModel model = new HydrantModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        ds.insertHydrant(model);
        ds.close();
    }

    public static void updateData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        HydrantModel model = new HydrantModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        ds.updateHydrantData(model);
        ds.close();
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Hydrant Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("SAVE AND EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validationGenerate()) {
                    saveData();
                    mActivity.finish();
                }
            }
        });

        builder.show();
    }

}
