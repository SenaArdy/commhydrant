package com.ptfi.commhydrant.Utils;

/**
 * Created by senaardyputra on 7/19/16.
 */
public class LanguageConstants {

    public String TOGGLE_BTN = "Indonesia";
    public String Q1 = "Instalasi selesai";
    public String Q2 = "Memakai bahan standar FSME";
    public String Q3 = "Test fungsional";
    public String Q4 = "Ada penggambaran";


    public LanguageConstants(boolean isEnglish) {
        if (isEnglish) {
             TOGGLE_BTN = "English";
             Q1 = "Installation complete";
             Q2 = "Material use meet FSME standard";
             Q3 = "Function test";
             Q4 = "Drawing available";
        } else {
            TOGGLE_BTN = "Indonesia";
            Q1 = "Instalasi selesai";
            Q2 = "Memakai bahan standar FSME";
            Q3 = "Test fungsional";
            Q4 = "Ada penggambaran";
        }
    }
}
