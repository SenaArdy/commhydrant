package com.ptfi.commhydrant.Utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ptfi.commhydrant.Fragments.DatePickerFragment;
import com.ptfi.commhydrant.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by senaardyputra on 7/28/16.
 */
public class Helper {

    public static Uri fileUri;
    public static final int INSTALL_APP_CODE = 77;

    public enum Lookup {
        inspectorData
    }

    public static Lookup lookUp;

    public static ProgressDialog progressDialog;

    public static void createDirectory(String path) {
        if (path != null) {
            if (path.length() > 0) {
                if (!new File(path).exists()) {
                    new File(path).mkdirs();
                }
            }
        }
    }

    public static void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }

    public static void deleteAllFilesOnDirectory(String directoryPath) {
        try {
            delete(new File(directoryPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getAppVersion(Activity activity) {
        String appVersion = "";

        try {
            appVersion = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "Version of application is cannot be detected";
            e.printStackTrace();
        }

        return appVersion;
    }

    public static int getAppVersionCode(Activity activity) {
        int versionCode = -1;

        try {
            versionCode = (activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionCode;
    }

    public static void showDatePicker(View v, FragmentActivity context,
                                      DatePickerDialog.OnDateSetListener callBack, int year, int month,
                                      int day) {
        DatePickerFragment newFragment = new DatePickerFragment(context, callBack,
                year, month, day);
        newFragment.show(context.getSupportFragmentManager(), "datePicker");
    }

    // =========================================
    // Date
    // =========================================
    public static String formatNumber(int number) {
        if (number < 10) {
            return "0" + String.valueOf(number);
        } else {
            return String.valueOf(number);
        }
    }

    public static String numToMonthName(int num) {
        switch (num) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "January";
        }
    }

    public static void copyFile(InputStream in, OutputStream out)
            throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static void copyFileAsset(String fileName, String folder,
                                     Context activity) {
        AssetManager assetManager = activity.getResources().getAssets();
        String[] files = null;
        try {
            files = assetManager.list(FoldersFilesName.ROOT_FOLDER_NAME + "/"
                    + FoldersFilesName.APP_FOLDER_NAME + "/" + folder);
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        for (int i = 0; i < files.length; i++) {
            File importFolder = new File(FoldersFilesName.BASEPATH + "/"
                    + FoldersFilesName.APP_FOLDER_NAME + "/" + folder + "/" + fileName);
            if (files[i].equalsIgnoreCase(fileName) && !importFolder.exists()) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(FoldersFilesName.ROOT_FOLDER_NAME + "/"
                            + FoldersFilesName.APP_FOLDER_NAME + "/" + folder + "/"
                            + files[i]);
                    out = new FileOutputStream(FoldersFilesName.BASEPATH + "/"
                            + FoldersFilesName.APP_FOLDER_NAME + "/" + folder + "/"
                            + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    Log.e("copy file ERROR", e.toString());
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void copyFileFromAssets(String outputPath, String outputFile,
                                          String pathAsset, Activity activity, String fileName) {
        AssetManager assetManager = activity.getResources().getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(pathAsset + "/" + fileName);
            out = new FileOutputStream(outputPath + "/" + outputFile);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("ERROR", e.toString());
            e.printStackTrace();
        }
    }

    public static void copyAndOpenBarcodeScannerAPK(Activity context) {
        String outputPath = Environment.getExternalStorageDirectory() + "/"
                + ".tempApk";
        createDirectory(outputPath);
        copyFileFromAssets(outputPath, "BarcodeScanner-4.7.3.apk", "APK",
                context, "BarcodeScanner-4.7.3.apk");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.fromFile(new File(outputPath + "/"
                        + "BarcodeScanner-4.7.3.apk")),
                "application/vnd.android.package-archive");
        context.startActivityForResult(intent, INSTALL_APP_CODE);
    }

    public static void showPopUpMessage(final Activity activity,
                                        final String title, final String message, final View.OnClickListener oc) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_alert);

                TextView titleET = (TextView) dialog.findViewById(R.id.title);
                TextView messageET = (TextView) dialog
                        .findViewById(R.id.nameETHome);

                titleET.setText(title);
                messageET.setText(message);

                Button okButton = (Button) dialog.findViewById(R.id.button_ok);

                if (oc == null) {
                    okButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });

                } else {
                    okButton.setOnClickListener(oc);
                }
                dialog.show();
            }
        });
    }

    public static void showProgressDialog(Activity context) {
        progressDialog = ProgressDialog.show(context, "Loading",
                "Please wait..", true, false);
    }

    public static void showProgressDialog(Context context, String title,
                                          String message) {
        progressDialog = ProgressDialog.show(context, title, message, true,
                false);
    }

    public static void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    public class CustomProgressDialog extends Dialog {
        private Dialog dialog;
        private TextView titleET;
        private TextView contentET;
        private ProgressBar progressBar;
        private Activity activity;

        public CustomProgressDialog(Context context) {
            super(context);
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.popup_progress_import_dialog);

            titleET = (TextView) dialog.findViewById(R.id.title);
            contentET = (TextView) dialog.findViewById(R.id.contentProgressBar);
            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);

            activity = (Activity) context;
        }

        public Dialog getDialog() {
            return dialog;
        }

        public void setDialog(Dialog dialog) {
            this.dialog = dialog;
        }

        public void setTextProgress(final String message) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    contentET.setText(message);
                }
            });
        }

        public void setTextTitle(final String title) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    titleET.setText(title);
                }
            });
        }

        public void setProgress(final int progress) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });
        }
    }

    public static CustomProgressDialog getCostumProgressDialog(
            final Activity activity) {
        return new Helper().new CustomProgressDialog(activity);
    }

    public static boolean isAvailableIntent(Context ctx, Intent intent) {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list = mgr.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static String checkDuplicate(String fileName, String path,
                                        String extension) {
        int i = 0;
        boolean isContinueAc = true;
        while (isContinueAc) {
            File checkFile = new File(path,
                    fileName
                            + (i == 0 ? extension : "_" + String.valueOf(i)
                            + extension));

            if (checkFile.exists() && isContinueAc) {
                i++;
            } else
                isContinueAc = false;
        }

        fileName += (i == 0 ? extension : "_" + String.valueOf(i) + extension);
        return fileName;
    }

    public static void copyFolderAndContent(String destinationPath,
                                            String pathAsset, Activity activity) {
        AssetManager assetManager = activity.getResources().getAssets();
        String[] filesAsset = null;
        File folder = new File(destinationPath);
        try {
            filesAsset = assetManager.list(pathAsset);
            if (!folder.exists())
                folder.mkdirs();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < filesAsset.length; i++) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(pathAsset + "/" + filesAsset[i]);
                out = new FileOutputStream(destinationPath + "/"
                        + filesAsset[i]);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setPic(ImageView iv, String filePath) {
        if (filePath == null || filePath.equalsIgnoreCase("")) {
            iv.setImageResource(R.drawable.edit_image);
        } else {
            // Get the dimensions of the View
            int targetW = iv.getWidth();
            int targetH = iv.getHeight();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, bmOptions);

            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int scaleFactor;

            // Determine how much to scale down the image
            if (targetW == 0 || targetH == 0) {
                scaleFactor = 3;
            } else {
                scaleFactor = Math.min(photoW / targetW, photoH / targetH);
            }

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
            try {
                ExifInterface ei;
                ei = new ExifInterface(filePath);
                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateImage(bitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateImage(bitmap, 180);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap == null) {
                iv.setImageResource(R.drawable.edit_image);
            } else {
                iv.setImageBitmap(bitmap);
            }
        }
    }

    public static Bitmap rotateImage(Bitmap img, float degree) {
        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        Bitmap rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    public static Bitmap rotateImage(String filePath, float degree, int reqWidth) {
        Bitmap rotatedBMP = null;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        bmOptions.inSampleSize = 5;
        bmOptions.inPurgeable = true;
        Bitmap img;
        BitmapFactory.decodeFile(filePath, bmOptions);

        // Calculate inSampleSize
        bmOptions.inSampleSize = calculateInSampleSize(bmOptions, reqWidth);

        // Decode bitmap with inSampleSize set
        bmOptions.inJustDecodeBounds = false;
        img = BitmapFactory.decodeFile(filePath, bmOptions);

        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth) {
        // Raw height and width of image
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (width > reqWidth) {

            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /* =============================
                SIGNATURE
    ============================= */
    public static byte[] bitmapToByte(Bitmap bitmap) {
        if (bitmap == null)
            return null;
        else {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            return baos.toByteArray();
        }
    }

    public static Bitmap byteToBitmap(byte[] byteArray) {
        if (byteArray != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(
                    byteArray);
            return BitmapFactory.decodeStream(imageStream);
        } else {
            return null;
        }
    }

    public static Bitmap getBitmapFromFile(String imagePath) {
        Bitmap bitmap = null;

        if (imagePath != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 8;
            bitmap = BitmapFactory.decodeFile(imagePath, options);
        }

        return bitmap;
    }

    public static Bitmap rotateImageAccordingToImageOrientation(
            String imagePath, int reqWidth) {
        Bitmap bitmap = null;
        if (imagePath != null) {
            try {
                ExifInterface ei;
                ei = new ExifInterface(imagePath);
                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = Helper.rotateImage(imagePath, 90, (int) reqWidth);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = Helper.rotateImage(imagePath, 180, (int) reqWidth);
                        break;
                    default:
                        bitmap = Helper.rotateImage(imagePath, 0, (int) reqWidth);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }
}
