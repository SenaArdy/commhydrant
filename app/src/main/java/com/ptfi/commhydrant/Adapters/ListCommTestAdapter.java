package com.ptfi.commhydrant.Adapters;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.commhydrant.Database.DataSource;
import com.ptfi.commhydrant.Fragments.HydrantFragment;
import com.ptfi.commhydrant.Models.CommissioningModel;
import com.ptfi.commhydrant.Models.ComplianceModel;
import com.ptfi.commhydrant.PopUp.PopUp;
import com.ptfi.commhydrant.PopUp.PopUpCommTestCallBack;
import com.ptfi.commhydrant.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 7/30/16.
 */
public class ListCommTestAdapter extends RecyclerView.Adapter<ListCommTestAdapter.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<CommissioningModel> commissioningData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView commTestTV;
        TextView remarksTV;
        TextView noTV;
        TextView statusTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.noTV = (TextView) itemView.findViewById(R.id.noTV);
            this.commTestTV = (TextView) itemView.findViewById(R.id.commTestTV);
            this.statusTV = (TextView) itemView.findViewById(R.id.statusTV);
            this.remarksTV = (TextView) itemView.findViewById(R.id.remarksTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String equipment = commissioningData.get(position).getEquipment();
            String date = commissioningData.get(position).getDate();
            final String register = commissioningData.get(position).getRegister();
            final String question = commissioningData.get(position).getQuestion();
            final String answer = commissioningData.get(position).getAnswer();
            final String remark = commissioningData.get(position).getRemarks();

            PopUp.showCommTest(mActivity, equipment, date, register, question, answer, remark, false, new PopUpCommTestCallBack() {
                @Override
                public void onDataSave(CommissioningModel model) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    ds.updateCommTest(model, question, answer, remark);
                    HydrantFragment.commTestRv();

                    ds.close();
                }
            });
        }
    }

    public ListCommTestAdapter(Activity mActivity, ArrayList<CommissioningModel> commissioningData) {
        super();
        this.mActivity = mActivity;
        this.commissioningData = commissioningData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_commissioning_test, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView noTV = holder.noTV;
        TextView commTestTV = holder.commTestTV;
        TextView remarksTV = holder.remarksTV;
        TextView statusTV = holder.statusTV;

        commTestTV.setText(commissioningData.get(position).getQuestion());
        remarksTV.setText(commissioningData.get(position).getRemarks());
        noTV.setText(Integer.toString(position + 1) + ".");
        statusTV.setText(commissioningData.get(position).getAnswer());
        if (commissioningData.get(position).getAnswer().equalsIgnoreCase("Pass")) {
            statusTV.setTextColor(mActivity.getResources().getColor(R.color.primaryGreen));
        } else if (commissioningData.get(position).getAnswer().equalsIgnoreCase("No")){
            statusTV.setTextColor(mActivity.getResources().getColor(R.color.colorRedAccent));
        } else {
            statusTV.setTextColor(mActivity.getResources().getColor(R.color.grayContainer));
        }
    }

    @Override
    public int getItemCount() {
        return commissioningData.size();
    }
}
